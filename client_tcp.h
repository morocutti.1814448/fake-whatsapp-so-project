//FS - Contiene la struct per le informazioni del client usata dal server

typedef struct client_tcp{
	int sockfd;                     //fd del socket
    struct sockaddr_in address;     //ip client
	int id;                        	//id del client
	char name[50];                  //nome utente del client
	bool richiesta_conn;			//richiesta connessione nuovi utenti
	struct client_tcp* next;				//puntatore al successivo, NULL se il successivo e' vuoto
} client_tcp;
