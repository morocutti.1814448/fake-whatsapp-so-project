//FS - Contiene la struttura dati che usera' il client per il thread di invio messaggi lato CLIENT
#include "msg.h"

typedef struct server_msg{
    struct sockaddr_in server;  //informazioni del server
    int sockfd;                 //fd del socket

}server_msg;