//FS - Include librerie
#include <sys/socket.h>
#include <arpa/inet.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>	//LM - per gestione segnale ctrl-c

//Include strutture dati
#include "common.h"
#include "server_msg.h"
#include "info_msg.h"

//Variabili Globali
int flag=1;
int flagUDP=1;
int sockUDP;
int sockTCP;

//Segnatura funzioni
void ErrorHandler(char* errorMessage);				//Funzione per la gestione degli errori
void catch_ctrl_c_and_exit(int sig);				//Funzione per catturare il segnale
void str_print();									//Funzione per la sistemazione della stampa
void str_change(char* arr, int length);				//Funzione per sostituire il terminatore di stringa
void cripta(char *arg, int size);					//Funzione per criptare il messaggio da inviare
void decripta(char *arg, int size);					//Funzione per decriptare il messaggio ricevuto
void* ricezione_messaggio(void* arg);				//Thread per la ricezione dei messaggi

//LM - Funzione per la stampa di errori con la terminazione di essi
void ErrorHandler(char* errorMessage){
	printf("%s\n", errorMessage);
	close(sockTCP);
	close(sockUDP);
	exit(EXIT_FAILURE);
}

//FS - Funzione per catturare il segnale e gestirlo
void catch_ctrl_c_and_exit(int sig){
	flag = 0;
	flagUDP = 0;
}

//FS - funzione per pulizia dell'output
void str_print() {
  printf("%s", "> ");
  fflush(stdout);
}

//PS - Funzione per sostituire \n con \0 (terminatore di stringa)
void str_change(char* arr, int length) {
	int i;
	for (i=0; i<length; i++) {
		if (arr[i]=='\n') {
			arr[i] = '\0';
			break;
		}
	}
}

//PS - Funzione per criptare le stringhe che poi verranno inviate al server
void cripta(char *arg, int size){
	int i;
	for(i=0; i<size; i++){
		arg[i]+=2;
	}
}

//FS - Funzione per decriptare le stringhe ricevute dal server
void decripta(char *arg, int size){
	int i;
	for(i=0; i<size; i++){
		arg[i]-=2;
	}
}

//LM - Start Routine del thread per la ricezione dei messaggi 
void* ricezione_messaggio(void* arg){

	server_msg *invio=(server_msg *)arg; 
	int respStringLen;
	unsigned int fromSize;
	msg messaggio_ricevuto;

	while(flagUDP){
		//PS - Ciclo di ricezione dei messaggi da parte di un altro client
		//Client riceve la struct messaggio che ha inviato l'altro utente
		fromSize = sizeof(&invio->server);
		respStringLen = recvfrom(invio->sockfd, (struct msg*)&messaggio_ricevuto, sizeof(messaggio_ricevuto), 0, (struct sockaddr*)&invio->server, &fromSize);
		if(respStringLen == -1){
			ErrorHandler("[ERRORE]: Ricezione UDP.\n");
		}
		//FS - Stampa del messaggio
		if(strcmp(messaggio_ricevuto.head.utente_sorg,"SERVER") == 0){
			if (strcmp(messaggio_ricevuto.messaggio.mess, "Server esploso!") == 0){
				catch_ctrl_c_and_exit(1);
				ErrorHandler("[AVVISO]: Server esploso!\n");
			}
			if (strcmp(messaggio_ricevuto.messaggio.mess, "Utente ora offline") == 0){
				catch_ctrl_c_and_exit(1);
				ErrorHandler("[AVVISO]: Destinatario Offline\n");
			}
		}
		if(strcmp(messaggio_ricevuto.head.utente_sorg, "ACK"))
			decripta(messaggio_ricevuto.messaggio.mess, strlen(messaggio_ricevuto.messaggio.mess));
	
		messaggio_ricevuto.messaggio.mess[respStringLen] = '\0';
		printf("%s: %s\n", messaggio_ricevuto.head.utente_sorg, messaggio_ricevuto.messaggio.mess);
		str_print();
		
		//LM - Invio al server il secondo ACK, ho ricevuto il mess
		if(messaggio_ricevuto.head.stato==2){
			messaggio_ricevuto.head.stato=3;
			sprintf(messaggio_ricevuto.messaggio.mess, "Ho ricevuto il mess (%s)", messaggio_ricevuto.head.utente_dest);
			strcpy(messaggio_ricevuto.head.utente_sorg, "ACK");
				if(sendto(invio->sockfd, (struct msg *)&messaggio_ricevuto, sizeof(messaggio_ricevuto), 0, (struct sockaddr*)&invio->server, sizeof(invio->server)) < 0)
					ErrorHandler("[ERRORE]: sendto() per 2 ACK fallito");
		}
	}
	pthread_exit(0);
}


int main(){

	//Gestore del segnale
	struct sigaction act;
	act.sa_handler=catch_ctrl_c_and_exit;
	sigaction(SIGINT, &act, NULL);

	struct sockaddr_in ServAddrTCP;
	msg invio_msg;				//Il messaggio da inviare (UDP)

	char risposta[ECHOMAX];		//risposta che viene inviata al server (TCP)
	server_msg invio;
	info_msg msg_rcv;

	//PS - Spostate variabili per UDP
	char mesgString[ECHOMAX];
	char dest[50];
	int msgStringLen;

	if((sockTCP = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		ErrorHandler("[ERRORE]: Creazione socket TCP fallita.");

	//Specifiche indirizzo server TCP
	memset(&ServAddrTCP, 0, sizeof(ServAddrTCP));
	ServAddrTCP.sin_family= AF_INET;
	ServAddrTCP.sin_port= htons(PORTA);
	ServAddrTCP.sin_addr.s_addr = inet_addr(INDIRIZZO_IP);

	int err = connect(sockTCP, (struct sockaddr *)&ServAddrTCP, sizeof(ServAddrTCP));
  	if (err == -1)
		ErrorHandler("[ERRORE]: Connessione rifiutata.\n");

	struct sockaddr_in ServAddrUDP;

	//FS - Variabile per momorizzare se e' stata instaurata un connessione UDP o no
	int fUDP=0;

	//LM - Creazione thread per invio messaggio
	pthread_t send_msg_thread;

	printf("///// BENVENUTO IN FAKE WHATSAPP! /////\n\n");

	int x=0;
	//FS - Creazione di un while unico per la ricezione dei messaggi
	while(flag==1){
		//PS - Pulizia del buffer stdin per evitare problemi
		fflush(stdin);
		//FS - Pulizia del buffer
		bzero((struct info_msg *)&msg_rcv,sizeof(msg_rcv));
		//FS - Ricezione messaggi dal server
		if(recv(sockTCP, (struct info_msg *)&msg_rcv, sizeof(msg_rcv), 0) > 0)
			printf("[SERVER]: %s\n" , msg_rcv.info);
		else{
			if(fUDP==1){
				//LM - Premuto ctrl-c;
				strcpy(invio_msg.head.utente_dest, "SERVER");
				strcpy(invio_msg.messaggio.mess, "ADDIO");
				if(sendto(sockUDP, (struct msg *)&invio_msg, sizeof(invio_msg), 0, (struct sockaddr*)&ServAddrUDP, sizeof(ServAddrUDP)) < 0)
					ErrorHandler("[ERRORE]: Sendto() per disconnessione ha fallito");
				catch_ctrl_c_and_exit(1);
				ErrorHandler("[INFO]: Disconnesso!\n");
			}
			else{
				printf("[SERVER]: Server Esploso!\n");
				catch_ctrl_c_and_exit(1);
				ErrorHandler("[INFO]: Disconnesso!\n");
			}
		}
		switch (msg_rcv.id){
			case -1:
				catch_ctrl_c_and_exit(1);
				ErrorHandler("[AVVISO]: ADDIO\n");
			case 1:
				strcpy(invio_msg.head.utente_sorg, risposta);
				fgets(risposta, ECHOMAX, stdin);	//FS - sostituzione di scanf on fgets per preblemi di inserimento 
				str_change(risposta, sizeof(risposta));
				if(send(sockTCP, risposta, sizeof(risposta), 0) < 0)
					ErrorHandler("[ERRORE]: Invio fallito.\n");
				continue;
			case 2:									//Accesso riuscito 
				//FS - spostate setup UDP
				if((sockUDP = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
					ErrorHandler("[ERRORE]: Creazione socket UDP fallita.\n");

				//LM - Set dei vari parametri del server UDP
				memset(&ServAddrUDP, 0, sizeof(ServAddrUDP));
				ServAddrUDP.sin_family= AF_INET;
				ServAddrUDP.sin_addr.s_addr = inet_addr(INDIRIZZO_IP);
				ServAddrUDP.sin_port= htons(PORTA);

				//LM - Assegnazione valori alla struct che usera il thread
				invio.server = ServAddrUDP;
				invio.sockfd = sockUDP;
				//LM - metto come destinatario il server
				strcpy(invio_msg.head.utente_dest, "SERVER");
				bzero(mesgString, sizeof(mesgString));
				//LM - sostituire utente con il nome utente
				sprintf(mesgString, "%s connesso.", invio_msg.head.utente_sorg);
				strcpy(invio_msg.messaggio.mess, mesgString);
				if(sendto(sockUDP, (struct msg *)&invio_msg, sizeof(invio_msg), 0, (struct sockaddr*)&ServAddrUDP, sizeof(ServAddrUDP)) < 0)
					ErrorHandler("[ERRORE]: Sendto() per salvere utente ha fallito");
				if(pthread_create(&send_msg_thread, NULL, ricezione_messaggio, (void *)&invio) != 0)
					printf("[ERRORE]: Creazione thread per ricezioni messaggi fallito.\n");	
				fUDP=1;		
				continue;
			case 3:									//Accesso fallito
				ErrorHandler("[ERRORE]: Accesso fallito!\n");
			case 4:									//Utenti online o attesa connessioni
				//FS - Recupero informazioni messagio e stampa utenti online
				x=msg_rcv.online.size;
				if(x>1){
					printf("%d " , x);
					printf("utenti online : \n");
					for(int i=0;i<x;i++){
						printf("%s\t",msg_rcv.online.nome[i]);
					}
					bzero(risposta,sizeof(risposta));
					printf("\n");
					//PS - Possibilita' di aggiornare la lista degli utenti con chi si vuole chattare
					printf("Inserisci l'utente con cui vuoi chattare oppure 'aggiorna' per aggiornare:\n");
					fgets(risposta, ECHOMAX, stdin);
					if(strcmp(risposta, "aggiorna") == 0){
						printf("Attesa connessioni...\n");
					}
					str_change(risposta, sizeof(risposta));
					//FS - Send per utente selezionato
					if(send(sockTCP, risposta, sizeof(risposta), 0) < 0)
						ErrorHandler("[ERRORE]: Invio utente selezionato fallito.\n");
				}
				else{
					//FS - Il primo client aspetta un altra connessione
					bzero(risposta,sizeof(risposta));
					strcpy(risposta, "aggiorna");
					if(send(sockTCP, risposta, sizeof(risposta), 0) < 0)
						ErrorHandler("[ERRORE]: Invio aggiorna utente fallito.\n");
					printf("In attesa di nuove connessioni...\n");
				}
				continue;
			case 5:									//inzio chat con Utente selezionato quindi vado nella parte UDP 
				strcpy(invio_msg.head.utente_dest, risposta);
				flag = 0;
				continue;
			case 6:					//Chat con se stesso
			case 7:					//Utente selezionato non online
			case 8:					//Utente non registrato
				flag=0;
				flagUDP=0;
				continue;
			case 9:
				break;
			default:
				fgets(risposta, ECHOMAX, stdin);	//FS - sostituzione di scanf on fgets per preblemi di inserimento 
				str_change(risposta, sizeof(risposta));
				if(send(sockTCP, risposta, sizeof(risposta), 0) < 0)
					ErrorHandler("[ERRORE]: Invio fallito.\n");
				continue;
		}
	}

	if(flagUDP==0){
		if(fUDP==1){
			//Premuto ctrl-c;
			strcpy(invio_msg.head.utente_dest, "SERVER");
			strcpy(invio_msg.messaggio.mess, "ADDIO");
			if(sendto(sockUDP, (struct msg *)&invio_msg, sizeof(invio_msg), 0, (struct sockaddr*)&ServAddrUDP, sizeof(ServAddrUDP)) < 0)
				ErrorHandler("[ERRORE]: Sendto() per disconnessione ha fallito");
			catch_ctrl_c_and_exit(1);
			ErrorHandler("[INFO]: Disconnesso!\n");
		}
		else{
			catch_ctrl_c_and_exit(1);
			ErrorHandler("[AVVISO]: ADDIO\n");
		}
	}

	//PS - salvo il destinatario
	bzero(dest, sizeof(dest));
	strcpy(dest, invio_msg.head.utente_dest);

	//PS - setto come destinatario l'utente selezionato
	strcpy(invio_msg.head.utente_dest, dest);
	str_print();
	bzero(mesgString, sizeof(mesgString));
	fflush(stdin);

	while(fgets(mesgString, ECHOMAX, stdin) != NULL){ //FS - sostituzione di scanf con fgets per includere spazi vuoti nelle stringhe
		str_change(mesgString, sizeof(mesgString));
		cripta(mesgString, strlen(mesgString));
		if((msgStringLen = strlen(mesgString)) > ECHOMAX)
			ErrorHandler("[ERRORE]: Stringa troppo lunga!");
		//LM - Invio messaggio al server che poi lo inoltra a utente_dest
		strcpy(invio_msg.messaggio.mess, mesgString);
		invio_msg.head.stato=1;
		if(sendto(sockUDP, (struct msg *)&invio_msg, sizeof(invio_msg), 0, (struct sockaddr*)&ServAddrUDP, sizeof(ServAddrUDP)) < 0)
			ErrorHandler("[ERRORE]: sendto() ha fallito");
		bzero(mesgString, sizeof(mesgString));
		fflush(stdin);
		str_print();
	}
	//Premuto ctrl-c;
	strcpy(invio_msg.head.utente_dest, "SERVER");
	strcpy(invio_msg.messaggio.mess, "ADDIO");
	if(sendto(sockUDP, (struct msg *)&invio_msg, sizeof(invio_msg), 0, (struct sockaddr*)&ServAddrUDP, sizeof(ServAddrUDP)) < 0)
		ErrorHandler("[ERRORE]: Sendto() per disconnessione ha fallito");
	catch_ctrl_c_and_exit(1);

	return 0;
}
