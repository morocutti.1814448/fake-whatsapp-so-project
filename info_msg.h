//FS - Struttura dati che conterra' i messaggi e gli id che inviera il server al client

#include "utenti_online.h"

typedef struct info_msg{
    unsigned int id;    //id del messaggio
    //-1 - Chiusura server 
    // 0 - Inserimento nome / 1 - Inserimento pass / 2 - Accesso eseguito / 3 - Accesso Fallito / 4 - invio utenti online / 5 - inzio chat
    // 6 - Non puoi parlare con te stesso / 7 - Utente non online / 8 - Utente non registrato / 9 - ERRORE chiusura / 10 - Richiesta Chat
    char info[500];     //messaggio
    utenti_online online;
} info_msg;
