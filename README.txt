PROGETTO: "Private chat"

NOME: "Fake Whatsapp"

COMPONENTI GRUPPO: Leonardo Morocutti (1814448), Fabio Sestito (1815479), Paolo Saquella (1806333)

WHAT: Il progetto consiste in una Chat Privata per lo scambio di messaggi tra due utenti, con la possibilità di ricevere messaggi da altri utenti oltre il destinatario scelto per i propri.

HOW: L'applicazione è composta da due parti principali:

	-Server: Rimane in attesa delle connessioni e gestisce il Login degli utenti tramite TCP. Inoltre riceve tutti i messaggi inviati dai 			client autenticati e si occupa dell'inoltro degli stessi ai destinatari giusti tramite UDP, preoccupandosi di inviare i 		relativi ACK;

	-Client: All'avvio si connette al Server tramite TCP e per effettuare il Login. Dopo aver effettuato il Login, è possibile scegliere 			tra gli utenti online il destinatario dei propri messaggi, i quali saranno inviati al Server tramite UDP dopo essere stati 			criptati.


HOW TO RUN: Per la compilazione è presente un Makefile. Per l'esecuzione di Server e Client è sufficiente lanciare gli eseguibili senza 	argomenti aggiuntivi.
