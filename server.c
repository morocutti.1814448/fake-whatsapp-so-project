//FS - Include librerie
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <signal.h>

//FS - Include strutture dati
#include "msg.h"
#include "utente.h"
#include "client_tcp.h"
#include "common.h"
#include "client_udp.h"
#include "cli_param.h"
#include "info_client.h"
#include "info_msg.h"

//LM
#define MAXCHAR 1000

//Segnatura funzioni
void ErrorHandler(char* errorMessage);									//Funzione per gestire l'errore
void catch_ctrl_c_and_exit(int sig);									//Funzione per gestire il segnale
int leggifile(char* nome, user* utente);								//Funzione che legge il DB da file
void insert_TCP(client_tcp* item);										//Funzione che insierisce dentro la lista dei client TCP
void insert_UDP(client_udp* item);										//Funzione che insierisce dentro la lista dei client UDP
void invio_utenti(info_msg arg);										//Funzione per l'invio agli utenti connessi quelli online
void stampa_TCP(client_tcp* TCP);										//Funzione per la stampa della lista dei client TCP
void stampa_UDP(client_udp* UDP);										//Funzione per la stampa della lista dei client UDP
void offline(user* utente, char* nome);									//Funzione per rendere offline l'utente che si e' disconnesso
void disconnetti_TCP(client_tcp** TCP, user* utente, char* nome);		//Funzione per la disconnessione dell'utente in TCP
void disconnetti_UDP(client_udp** UDP, user* utente, char* nome);		//Funzione per la disconnesisone dell'utente in UDP
int size_TCP(client_tcp* TCP);											//Funzione per la grandezza della lista TCP
int size_UDP(client_udp* UDP);											//Funzione per la grandezza della lista UDP
void richiesta_connessioni(client_tcp* TCP, char* nome);				//Funzione per sapere quale client ha richiesto delle nuove connessioni tramite aggiorna
void* login(void* arg);													//Thread per il login
void* thread_login(void* arg);											//Thread che rimane in ascolto per riceve le nuove connessioni

//Variabili Globali
int id = 0;			//id utente
pthread_mutex_t disconnect_TCP = PTHREAD_MUTEX_INITIALIZER, disconnect_UDP = PTHREAD_MUTEX_INITIALIZER,login_mutex = PTHREAD_MUTEX_INITIALIZER;
client_tcp* clientTCP;	//FS - Creazione lista di client TCP
client_udp *clientUDP;	//FS - Creazione lista di client UDP


//FS - Funzione per gestire ctrl+c
void catch_ctrl_c_and_exit(int sig){
	fprintf(stderr,"\nChiusura server e client connessi...\n");
	info_msg err;
	msg errUDP;
	strcpy(err.info,"Server esploso!");
	strcpy(errUDP.messaggio.mess,"Server esploso!");
	strcpy(errUDP.head.utente_sorg,"SERVER");
	err.id=-1;
	while(clientTCP!=NULL){
		if(send(clientTCP->sockfd, (struct info_msg *)&err, sizeof(err), 0) < 0)
			ErrorHandler("[ERRORE]: Send chiusura client fallita.");
		close(clientTCP->sockfd);
		clientTCP=clientTCP->next;	
	}
	while(clientUDP!=NULL){
		strcpy(errUDP.head.utente_dest,clientUDP->name);
		if(sendto(clientUDP->fd, (struct msg *)&errUDP, sizeof(errUDP), 0, (struct sockaddr *)&clientUDP->indirizzo, sizeof(clientUDP->indirizzo)) < 0)
			ErrorHandler("[ERRORE]: Send chiusura client UDP fallita.");
		clientUDP=clientUDP->next;			
	}
	free(clientTCP);
	free(clientUDP);
	ErrorHandler("[AVVISO]: Bye Bye!\n");
} 

//LM - Funzione per la stampa di errori con la terminazione di essi
void ErrorHandler(char* errorMessage){
	printf("%s\n",errorMessage); //aggiunta LM
	exit(EXIT_FAILURE);
}

//PS - Funzione per leggere da file
int leggifile(char* nome, user* utente){

	//LM Controllo per contare le righe == numero utenti nel file
	FILE *fp;
	char str[MAXCHAR];
	int count = 0;
	
	fp = fopen(nome, "r");
	if (fp == NULL){
    		return -1;
	}
	int j = 0;
	while (fgets(str, MAXCHAR, fp) != NULL) {
		int d = 0;
		char *ptr = strtok(str, ",");
		while(ptr != NULL){
			if(d == 0){
				strcpy(utente[j].username, ptr);
			}
			else{
				strcpy(utente[j].password, ptr);
			}
			ptr = strtok(NULL, "\n");
			d++;
		}
		utente[j].online=false;
		j++;
	}
	fclose(fp);
	return count;
}

//Funzione per inserimento nuove utente in TCP
void insert_TCP(client_tcp* item){
	struct client_tcp* temp=(struct client_tcp *)malloc(sizeof(struct client_tcp));
	temp = item;
	temp->next=NULL;
	if(clientTCP==NULL){
		clientTCP=temp;
		//fprintf(stderr,"Inserito %s, FD %d \n", temp->name, temp->sockfd);
	}
	else{
		struct client_tcp* TCP;
		TCP=clientTCP;
		while(TCP->next!=NULL){
			TCP=TCP->next;
		}
		TCP->next=temp;
		//fprintf(stderr,"Inserito %s FD %d \n", temp->name, temp->sockfd);
	}
}

//Funzione inserimento nuovo utente in UDP
void insert_UDP(client_udp* item){
	client_udp* temp=(client_udp *)malloc(sizeof(client_udp));
	temp = item;
	temp->next=NULL;
	if(clientUDP==NULL){
		clientUDP=temp;
	}
	else{
		client_udp* UDP;
		UDP=clientUDP;
		while(UDP->next!=NULL){
			UDP=UDP->next;
		}
		UDP->next=temp;
	}
}

//Funzione per inviare gli utenti online ai client connessi
void invio_utenti(info_msg arg){
	client_tcp *TCP = clientTCP;
	while(TCP){
		if(TCP->richiesta_conn==true){
			//printf("Invio a %s, Fd %d\n", TCP->name, TCP->sockfd);
			if(send(TCP->sockfd, (struct info_msg *)&arg, sizeof(arg), 0) < 0)
				ErrorHandler("[ERRORE]: Send per utenti online fallita.");
			TCP->richiesta_conn=false;
		}
		TCP=TCP->next;
	}
}

//Funzione per stamapre la lista dei client TCP connessi
void stampa_TCP(client_tcp* TCP){
	while(TCP){
		printf("[LISTA TCP]: Nome %s fd %d\n", TCP->name, TCP->sockfd);
		TCP=TCP->next;
	}
}

//Funzione per stamapre la lista dei client UDP connessi
void stampa_UDP(client_udp* UDP){
	while(UDP){
		printf("[LISTA UDP]: Nome %s\n", UDP->name);
		UDP=UDP->next;
	}
}

//Funzione per rendere offline l'utente che si e' disconneso
void offline(user* utente, char* nome){
	int i;
	for(i=0;i<N_UTENTI;i++){
		if(strcmp(utente[i].username,nome)==0){
			fprintf(stderr, "[AVVISO]: Offline: %s\n", utente[i].username);
			utente[i].online=false;
		}
	}
}

//Funzione per la disconnessione del client TCP
void disconnetti_TCP(client_tcp** TCP, user* utente, char* nome){

	client_tcp *temp = *TCP;
	//Primo elemento
	if(strcmp(temp->name,nome) == 0){
		fprintf(stderr, "[AVVISO]: Disconnessione TCP nome: %s\n", temp->name);
		*TCP = temp->next;
		free(temp);
		id--;
		offline(utente, nome);
	}
	else{
		client_tcp *prev = NULL;
		while(temp && strcmp(temp->name,nome)){
			prev=temp;
			temp=temp->next;
		}
		fprintf(stderr, "[AVVISO]: Disconnessione TCP nome: %s\n", temp->name);
		prev->next=temp->next;
		free(temp);
		id--;
		offline(utente, nome);
	}
	stampa_TCP(*TCP);
}

//Funzione per la disconnessione del client UDP
void disconnetti_UDP(client_udp** UDP, user* utente, char* nome){

	client_udp* temp = *UDP;
	//Primo elemento
	if((strcmp(temp->name,nome) == 0)){
		fprintf(stderr, "[AVVISO]: Disconnessione UDP nome: %s\n", temp->name);
		*UDP=temp->next;
		free(temp);
		disconnetti_TCP(&clientTCP, utente, nome);
	}
	else{
		client_udp* prev=NULL;
		while(temp && strcmp(temp->name,nome)){
			prev=temp;
			temp=temp->next;	
		}
		fprintf(stderr, "[AVVISO]: Disconnessione UDP nome: %s\n", temp->name);
		prev->next=temp->next;
		free(temp);
		disconnetti_TCP(&clientTCP, utente, nome);
	}
	stampa_UDP(clientUDP);
}

//Funzione per la grandezza della lista TCP								
int size_TCP(client_tcp* TCP){
	int size=0;
	while(TCP){
		size++;
		TCP=TCP->next;
	}
	return size;
}

//Funzione per la grandezza della lista UDP
int size_UDP(client_udp* UDP){
	int size=0;
	while(UDP){
		size++;
		UDP=UDP->next;
	}
	return size;
}

//Funzione che aggiorna chi ha fatto le nuove richiesta di connessione
void richiesta_connessioni(client_tcp* TCP, char* nome){
	info_msg invia;
	bzero((struct info_msg *)&invia,sizeof(invia));
	int size = 0;
	int g=0;
	size=size_TCP(clientTCP);
	//printf("SIZE TCP %d \n", size);
	while(TCP){
		if(strcmp(TCP->name, nome)==0){
			printf("[INFO]: %s ha richiesto nuove connessioni...\n", TCP->name);
			TCP->richiesta_conn=true; 
		}
		if(size>1){
			strcpy(invia.online.nome[g],TCP->name);
			g++;
		}
		TCP=TCP->next;
	}
	if(size>1){
		invia.id=4;
		invia.online.size=g;
		invio_utenti(invia);
	}
	
}

//PS - Start Routine passata al Thread per l'esecuzione della parte del login
void* login(void* arg){
	
	info_client *client_info = (info_client*)arg;
	char userName[ECHOMAX];
	char risp[ECHOMAX];
	bool nome = false;			//FS - controllo se l'utente e' registrato
	bool utenteOk = false;		//FS - controllo per l'utente selezionato
	info_msg invia;
	struct client_tcp *item_TCP;	//variabile per inserimento utente appena connesso

	bzero((struct info_msg *)&invia,sizeof(invia));
	strcpy(invia.info, "Inserire il nome utente: ");
	invia.id=0;
	if(send(client_info->sockfd, (struct info_msg *)&invia, sizeof(invia), 0) < 0)
		ErrorHandler("[ERRORE]: Send per Nome fallita!\n");
	//PS - Recive per il nome utente
	memset(userName,0,sizeof(userName));
	if(recv(client_info->sockfd, userName, sizeof(userName), 0) == -1){
		printf("[ERRORE]: Errore recv del nome.\n");
	}
	if(strlen(userName)==0 || !strcmp(userName,"\n")){
		printf("[AVVISO]: Disconnessione Client\n");
		pthread_exit(NULL);
	}
	//LM - For per ricezione nome
	for(int k = 0; k < N_UTENTI; k++){
		if(strcmp(userName, client_info->utente[k].username) == 0){
			//Nome utente valido
			//Controllo se e' gia online
			nome = true;
			client_tcp *punt_TCP=clientTCP;
			while(punt_TCP!=NULL){
				//se e' gia online
				if(strcmp(userName, punt_TCP->name) == 0){
					nome=false;
					printf("[ERRORE]: Utente gia loggato!\n");
					bzero((struct info_msg *)&invia,sizeof(invia));
					strcpy(invia.info, "Utente gia loggato!");
					invia.id = 8;
					if(send(client_info->sockfd, (struct info_msg *)&invia, sizeof(invia), 0) < 0) 
						ErrorHandler("[ERRORE]: Send per utente non registrato fallita");
				}
				punt_TCP=punt_TCP->next;
			}
			free(punt_TCP);
			if(!nome){
				pthread_exit(0);
			}
			printf("[INFO]: Nome utente valido: %s.\n", userName);
			//LM - Invio richiesta inserimento della password
			client_info->utente[k].online = true;
			bzero((struct info_msg *)&invia,sizeof(invia));
			strcpy(invia.info, "Inserire Password:");
			invia.id=1;
			if(send(client_info->sockfd, (struct info_msg *)&invia, sizeof(invia), 0) < 0)
				ErrorHandler("[ERRORE]: Send per la password fallita");
			
			//LM - Ricezione password dal client e controllo
			bzero(risp, sizeof(risp));
			if(recv(client_info->sockfd, risp, sizeof(risp), 0) == -1){
				printf("[ERRORE]: Errore recv della password.\n");
				client_info->utente[k].online = true;
			}
			if(strlen(risp)==0 || !strcmp(risp,"\n")){
				printf("[AVVISO]: Disconnessione Client.\n");
				client_info->utente[k].online=false;
				pthread_exit(NULL);
			}
			if(strcmp(risp, client_info->utente[k].password) == 0){
				printf("[INFO]: Password corretta.\n");
				//LM - Stringa per accesso eseguito
				bzero((struct info_msg *)&invia,sizeof(invia));
				strcpy(invia.info, "Accesso eseguito!");
				invia.id=2;
				if(send(client_info->sockfd, (struct info_msg *)&invia, sizeof(invia), 0) < 0)
					ErrorHandler("[ERRORE]: Send per l'accesso fallita");
				//FS - Salvataggio del client connesso 
				//FS - Mutex per problemi di accessi concorrenti
				pthread_mutex_lock(&login_mutex);
				//printf("Malloc TCP... ");
				item_TCP=(struct client_tcp *)malloc(sizeof(struct client_tcp));
				strcpy(item_TCP->name, userName);
				item_TCP->sockfd  =  client_info->sockfd;
				item_TCP->address = client_info->address;
				item_TCP->id = id;
				item_TCP->richiesta_conn=true;
				id++;
				insert_TCP(item_TCP);
				//printf("Completata!\n");
				fprintf(stderr,"[INFO]: Utente loggato %s\n", item_TCP->name);
				pthread_mutex_unlock(&login_mutex);
				stampa_TCP(clientTCP);
				//FS - Salvataggio degli utenti online
				bzero((struct info_msg *)&invia,sizeof(invia));
				invia.id = 4;
				client_tcp *punt=clientTCP;
				int g=0;
				while(punt!=NULL){
					strcpy(invia.online.nome[g],punt->name);
					g++;
					punt=punt->next;
				}
				free(punt);
				invia.online.size=g;
				printf("[INFO]: Utenti online: \n");
				for(int i=0;i<g;i++){
					printf("%s\t",invia.online.nome[i]);
				}
				printf("\n");
				//Invio a tutti i client connessi gli utenti online
				invio_utenti(invia);
				//FS - Recv per utente selezionato
				while(1){
					bzero(risp, sizeof(risp));
					if(recv(client_info->sockfd, risp, sizeof(risp), 0) == -1)
						printf("[ERRORE]: Errore recv utente selezionato.\n");
					if(strlen(risp)==0 || !strcmp(risp,"\n")){
						pthread_exit(NULL);
					}
					else{
						//FS - Se ha fatto una richiesta di connessione
						if(strcmp(risp, "aggiorna") == 0){
							richiesta_connessioni(clientTCP, userName);
						}
						else{
							//FS - Controllo per l'utente selezionato
							if(strcmp(risp, userName) == 0){
								printf("[INFO]: Chat avviata con se stesso.\n");
								//FS - Invio risposta di controllo per l'utente
								bzero((struct info_msg *)&invia,sizeof(invia));
								strcpy(invia.info, "Non puoi parlare con te stesso SCEMOOOO.");
								invia.id=6;
								utenteOk=true;
								if(send(client_info->sockfd, (void *)&invia, sizeof(invia), 0) < 0 )
									ErrorHandler("[ERRORE]: Send chat con te stesso fallita.\n");
								break;
							}
							else{
								//FS - Se l'utente mandato e' online
								client_tcp* TCP;
								TCP=clientTCP;
								while(TCP!=NULL){
									if(strcmp(TCP->name,risp) == 0){
										printf("[INFO]: %s inizia chat con %s \n", userName, risp);
										//FS - Invio risposta di controllo per l'utente
										utenteOk = true;
										bzero((struct info_msg *)&invia,sizeof(invia));
										sprintf(invia.info, "Inizio chat con %s", risp);
										invia.id = 5;
										if(send(client_info->sockfd, (struct info_msg *)&invia, sizeof(invia), 0) < 0 )
											ErrorHandler("[ERRORE]: Send inizio chat fallita.\n");
										break;
									}
									TCP=TCP->next;
								}
							}
							//FS - Utente non online
							if(!utenteOk){
								printf("[INFO]: Utente non online!\n");
								bzero((struct info_msg *)&invia,sizeof(invia));
								strcpy(invia.info, "Utente non online!");
								invia.id = 7;
								if(send(client_info->sockfd, (struct info_msg *)&invia, sizeof(invia), 0) < 0) 
									ErrorHandler("[ERRORE]: Send utente non oline fallita.\n");
								break;
							}
						}
					}
				}
			}
			
			//LM - else gestione errore password sbagliata
			else{ 
				printf("[ERRORE]: Password sbagliata\n");
				bzero((struct info_msg *)&invia,sizeof(invia));
				strcpy(invia.info, "Accesso Fallito!");
				invia.id = 3;
				if(send(client_info->sockfd, (struct info_msg *)&invia, sizeof(invia), 0) < 0)
					ErrorHandler("[ERRORE]: Send per accesso fallito fallito.\n");
				break;
			}
		}
	}		
	
	//LM - Controllo per vedere se l'utente è registrato
	if(!nome){
		printf("[ERRORE]: Utente non registrato!\n");
		bzero((struct info_msg *)&invia,sizeof(invia));
		strcpy(invia.info, "Utente non registrato!");
		invia.id = 8;
		if(send(client_info->sockfd, (struct info_msg *)&invia, sizeof(invia), 0) < 0) 
			ErrorHandler("[ERRORE]: Send per utente non registrato fallita");
	}
	pthread_exit(0);
}

//PS - Thread che rimane in ascolto di nuove richieste di connessione 
void* thread_login(void* arg){
	cli_param *cli = (cli_param*)arg;

	//Thread
	pthread_t thread_id;

	//Dichiaro l'attributo
	pthread_attr_t attr;

	//Inizializzo l'attributo
	pthread_attr_init(&attr);

	//Setto l'attributo specificando che il thread verrà creato in modo detached
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	while (1){
		//PS - Accept della connessione TCP
		socklen_t clilen = sizeof(cli->echoClntAddr);
		int connfd = accept(cli->socket, (struct sockaddr*)&cli->echoClntAddr, &clilen);
		printf("[INFO]: Connessione accettata.\n");

		//PS - Settaggio dei valori del client
		//FS - Allocazione con malloc per problemi di accesso alle varibili per il thread
		info_client *client_info= (info_client*)malloc(sizeof(info_client));
		client_info->address = cli->echoClntAddr;
		client_info->sockfd = connfd;
		memcpy(client_info->utente, cli->utente, sizeof(cli->utente));

		//Creazione del thread per il login
		if(pthread_create(&thread_id, &attr,  login, (void*)client_info) < 0){
			ErrorHandler("[ERRORE]: Creazione thread login fallita");
		}
	}

}

int main(int argc, char **argv){

	//Gestore del segnale
	struct sigaction act;
	act.sa_handler=catch_ctrl_c_and_exit;
	sigaction(SIGINT, &act, NULL);

	//LM - Lettura da file e popolamento array di utenti
	user utente[N_UTENTI];
	char* filename = "utente.txt";
	int ris=leggifile(filename, utente);
	//LM - Errore nel file
	if(ris == -1){
		printf("[ERRORE]: File %s non trovato!\n", filename);
	}

	printf("///// BENVENUTO IN FAKE WHATSAPP! (SERVER) /////\n\n");
	//LM - Stampa utenti registrati
	printf("[DATABASE]:\n");
	int x;
	for(x=0;x<N_UTENTI;x++){
		printf("-Nome: %s \t| Password: %s\n", utente[x].username, utente[x].password);
	}
	printf("\n");

	//LM - Creazione del socket in modo TCP per la connessione
	int sockTCP;

	if((sockTCP = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		ErrorHandler("[ERRORE]: Creazione socket TCP fallita.");
	printf("[INFO]: Socket TCP creato.\n");

	struct sockaddr_in ServAddrTCP;
	struct sockaddr_in ClntAddrTCP;

	ClntAddrTCP.sin_port=0;

	//Set dei vari parametri in TCP
	memset(&ServAddrTCP, 0, sizeof(ServAddrTCP));
	ServAddrTCP.sin_family= AF_INET;
	ServAddrTCP.sin_addr.s_addr = inet_addr(INDIRIZZO_IP);
	ServAddrTCP.sin_port= htons(PORTA);

	//Bind TCP
	if(bind(sockTCP, (struct sockaddr*)&ServAddrTCP, sizeof(ServAddrTCP)) < 0) {
    	ErrorHandler("[ERRORE]: Socket binding TCP fallita!");
  	}
	//Listen
	if (listen(sockTCP, 10) < 0) {
    	ErrorHandler("[ERRORE]: Socket listening TCP fallita");
	}
	
	//PS - Set dei vari parametri per la struttura dei parametri per i client
	cli_param *param = (cli_param *)malloc(sizeof(cli_param));
	param->echoClntAddr = ClntAddrTCP;
	param->socket = sockTCP;
	memcpy(param->utente, utente, sizeof(param->utente));

	pthread_t thread_princ;

	//PS - Creazione thread principale per l'ascolto dei nuovi utenti
	if(pthread_create(&thread_princ, NULL,  thread_login , (void*)param) < 0){
		ErrorHandler("[ERRORE]: creazione thread principale fallita");
	}

	//LM - Creazione socket UDP per i messaggi
	int sockUDP;
	
	int recvMsgSize;
	unsigned int recvSize;

	struct sockaddr_in ServAddrUDP;
	struct sockaddr_in ClntAddrUDP;

	if((sockUDP = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		ErrorHandler("[ERRORE]: Creazione socket UDP fallita.");
		
	//LM - Costruzione dell'indirizzo del server UDP
	memset(&ServAddrUDP, 0, sizeof(ServAddrUDP));
	ServAddrUDP.sin_family= AF_INET;
	ServAddrUDP.sin_addr.s_addr = inet_addr(INDIRIZZO_IP);
	ServAddrUDP.sin_port= htons(PORTA);

	//Bind della socketUDP
	if((bind(sockUDP, (struct sockaddr *)&ServAddrUDP, sizeof(ServAddrUDP))) < 0)
		ErrorHandler("[ERRORE]: Bind UDP fallita");
	
	//FS - Problema con porte dei client perche diverse da UDP a TCP
	int id_UDP=0;
	msg ricevuto;
	
	while(1){
		client_udp *item_UDP;		//nuovo utente in UDP
		client_udp *UDP = clientUDP;
		memset((struct msg *)&ricevuto, 0, sizeof(ricevuto)); //LM aggiunta struct messaggio da ricevere
		//printf("Sto aspettando il messaggio del client \n");
		recvSize=sizeof(ClntAddrUDP);
		recvMsgSize = recvfrom(sockUDP, (struct msg *)&ricevuto, sizeof(ricevuto), 0, (struct sockaddr*)&ClntAddrUDP, &recvSize);
		if(recvMsgSize == -1){
			ErrorHandler("[ERRORE]: Ricezione messaggio fallita!");
		}
		//LM - Contatore che controlla se l'utente non e' stato inserito nell struct di informazioni UDP
		int C=0;
		while(UDP!=NULL){
			if((UDP->indirizzo.sin_addr.s_addr != ClntAddrUDP.sin_addr.s_addr) || (UDP->indirizzo.sin_port != ClntAddrUDP.sin_port))
				C++;
			UDP=UDP->next;
		}
		//LM - Se l'utente non e' inserito, inserisci i vari campi
		if(C == id_UDP){
			item_UDP=(client_udp *)malloc(sizeof(client_udp));
			strcpy(item_UDP->name,ricevuto.head.utente_sorg);
			item_UDP->indirizzo = ClntAddrUDP;
			item_UDP->fd=sockUDP;
			item_UDP->id=id_UDP;
			id_UDP++;
			insert_UDP(item_UDP);
		}

		//Prima messaggio da parte di ogni client UDP ha come destinatario server, in modo da savarsi le informazioni
		if(strcmp(ricevuto.head.utente_dest, "SERVER") == 0){
			if(strcmp(ricevuto.messaggio.mess,"ADDIO") == 0){
				fprintf(stderr, "[INFO]: Sto disconnettendo %s\n", ricevuto.head.utente_sorg);
				disconnetti_UDP(&clientUDP, utente, ricevuto.head.utente_sorg);
				id_UDP--;
			}
			continue;
		}

		//Se il tipo di messaggio e' diverso da ACK, stampo il messaggio
		if(strcmp(ricevuto.head.utente_sorg, "ACK")!=0){
			printf("Da %s A %s\t", ricevuto.head.utente_sorg, ricevuto.head.utente_dest);
			printf("Messaggio: %s\n", ricevuto.messaggio.mess);
		}
		else{
			printf("Mandato ACK\n");
		}
		//PS - Scorri gli utenti in UDP e trova quello giusto 
		struct sockaddr_in clientDestinatario;
		clientDestinatario.sin_port=0;
		UDP=clientUDP;
		while(UDP!=NULL){
			if(strcmp(UDP->name, ricevuto.head.utente_dest) == 0){
				clientDestinatario = UDP->indirizzo;
				break;
			}
			UDP=UDP->next;
		}
		//stampa_UDP(clientUDP);
		//Riinvia la stringa messaggio ricevuto al client
		//FS - Mi serve l' indirizzo IP e porta dell'altro client connesso alla chat, salvato nella struttura clientUDP
		//FS - Se da errore, l' altro client UDP non e' piu online
		if(ricevuto.head.stato==1){
			if(sendto(sockUDP, (struct msg *)&ricevuto, sizeof(ricevuto), 0, (struct sockaddr *)&clientDestinatario, sizeof(clientDestinatario)) < 0){
				fprintf(stderr, "[INFO]: Destinatario offline!\n");
				strcpy(ricevuto.head.utente_dest, ricevuto.head.utente_sorg);
				strcpy(ricevuto.head.utente_sorg,"SERVER");
				strcpy(ricevuto.messaggio.mess,"Utente ora offline");
				if(sendto(sockUDP, (struct msg *)&ricevuto, sizeof(ricevuto), 0, (struct sockaddr*)&ClntAddrUDP, sizeof(ClntAddrUDP)) < 0)
					fprintf(stderr,"[ERRORE]: Invio informazioni al client sorg fallito!\n");
				disconnetti_UDP(&clientUDP, utente, ricevuto.head.utente_dest);
				id_UDP--;
			}
			//LM - Invio di ACK al client sorg
			else{
				ricevuto.head.stato=2;
				sprintf(ricevuto.messaggio.mess, "Invio messaggio a %s riuscito", ricevuto.head.utente_dest);
				strcpy(ricevuto.head.utente_sorg,"ACK");
				if(sendto(sockUDP, (struct msg *)&ricevuto, sizeof(ricevuto), 0, (struct sockaddr*)&ClntAddrUDP, sizeof(ClntAddrUDP)) < 0)
					fprintf(stderr,"[ERRORE]: Invio 1 ACK fallito!\n");
			}
		}
		else{
			if(sendto(sockUDP, (struct msg *)&ricevuto, sizeof(ricevuto), 0, (struct sockaddr*)&ClntAddrUDP, sizeof(ClntAddrUDP)) < 0)
				fprintf(stderr,"[ERRORE]: Invio ACK fallito!\n");
		}
		
	}

	return EXIT_SUCCESS;
}