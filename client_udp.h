//FS - Contiene la struttura dati per le informazioni riguardo alla connessioni in UDP lato SERVER

typedef struct client_udp{
    struct sockaddr_in indirizzo;   //ip client UDP  IP - PORTA
	int fd;							//file descriptor dela connessione
	int id;                        	//id del client
	char name[50];                  //nome utente del client UDP
	struct client_udp* next;				//puntatore al successivo, NULL altrimenti
} client_udp;
