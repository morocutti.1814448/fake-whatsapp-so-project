//FS - Contiene la struttura del messaggio, una parte header e una parte dati

#include "common.h"

//struttura header
typedef struct header{
    char utente_sorg[50];   //utente sorgente 
    char utente_dest[50];   //utente destinatario
    unsigned int stato;     //stato del messaggio
    //1 per invio al server - 2 per inoltro sul client - 3 ricezione dal client
} header;

typedef struct dati{
    char mess[ECHOMAX];     //campo per il mesaggio;
} dati;


typedef struct msg{
    header head;
    dati messaggio;
} msg;
